# List files installed by package

To retrieve a list of the files installed by a package:

```bash
$ pacman -Ql package_name
```
To retrieve a list of the files installed by a remote package:

```bash
$ pacman -Fl package_name
```
