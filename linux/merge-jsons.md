# Merge Jsons

To merge two json files one can use the following

```bash
$ jq -s '.[0] * .[1]' file1 file2 > output.json
```

The contents of file2 take precedence over the contents of file1
