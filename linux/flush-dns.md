# Flush DNS

Sometimes, some pesky website just won't update. Well here are some commands:

```bash
# Systemd
sudo systemd-resolve --flush
sudo systemd-resolve --statistics
sudo systemd-resolve --flush-caches

# nscd
sudo systemctl restart nscd

# dnsmasq
sudo systemctl restart dnsmasq

# named
sudo systemctl restart named

```
