# Makefile

To make a .PHONY imitator with the `touch` command

```make
VPATH
flags=.makeFlags
VPATH=$(flags)
$(shell mkdir -p $(flags))

test:
  touch $(flags)/$@
```

Running the above twice will result in:

```
$ make test
touch .makeFlags/test

$ make test
make: '.makeFlags/test' is up to date.
```
