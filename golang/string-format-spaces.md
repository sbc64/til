# String format spaces

A way of adding padding with spaces.

From [https://golang.org/pkg/fmt/](https://golang.org/pkg/fmt/):

`"-"	pad with spaces on the right rather than the left (left-justify the field)`

```go
fmt.Fprintf(W, "%-30s | MP |  W |  D |  L |  P\n", "Team")
```


The above pads with spaces on the left with width of 30.
