# Arrays and slices

[Go Slices: usage and internals](https://blog.golang.org/go-slices-usage-and-internals)

You can have the compiler count the array elements for you:

```go
b := [...]string{"Penn", "Teller"}
# is the same as
b := [2]string{"Penn", "Teller"}
```

