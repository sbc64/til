# Reading strings from io

```go
R := io.Reader
buf := new(bytes.Buffer)
buf.ReadFrom(R)
// Convert to string
stringValue := buf.String()
```

