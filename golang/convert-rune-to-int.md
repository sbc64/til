# Convert `rune` to `int`


An alternative to strconv.Atoi(string(n)) is int(n-'0') which should be safe after validating that n is a number with `unicode.IsNumber()`

```go
if !unicode.IsNumber(runeValue) {
  return false
}
//this
runeNumber, _ := strconv.Atoi(string(runeValue))
// same as this
runeNumber = int(runeValue - '0')
```
