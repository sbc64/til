# Byte handling

For examples, notice the single quote

```go
b := []byte{'T', 'e', 'a', 'm'}
stringByte := []byte("Team")
for idx := 0; idx < 27; idx++ {
       b = append(b, ' ')
}
```
