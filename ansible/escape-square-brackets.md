# Escape square brackets

To escape square brackets in ansible one needs to use the following syntax:
```yaml

- name: Get info on docker host and list images
  shell: "docker ps --format {{ '{{'}}.Names{{'}}' }}:"
  register: result
```

