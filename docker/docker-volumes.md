# Docker volumes 
 
Docker volumes in `Dockerfile` files are always generated in docker compose unless the docker-compose mounts the volume to the same path.
