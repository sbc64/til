#  Diffie–Hellman key exchange parameters

In nginx one can use `dhparams.pem` as the parameters for better
security. One can generate this file with the following command:

```bash
openssl dhparam -out dhparam.pem 4096
```

4096 bits is recommend to avoid the logjam attack: [https://weakdh.org/](https://weakdh.org/).
This file includes the prime __p__ and the generator __g__.

__IT IS SAFE TO MAKE THIS FILE PUBLIC__

You can learn more here: [https://www.youtube.com/watch?v=NmM9HA2MQGI](https://www.youtube.com/watch?v=NmM9HA2MQGI)

