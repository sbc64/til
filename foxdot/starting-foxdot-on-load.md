# Starting fox dot on load

After installing super collider and installing FoxDot using `Quarks.install("FoxDot")`

```bash
$ echo FoxDot.start >> ~/.config/SuperCollider/startup.scd
```
