# Basic GPG functions

Show all the keys including subkeys fingerprints

```bash
$ gpg -K --with-subkey-fingerprints
```

Edit a key with --expert flag

```bash
$ gpg --edit-key --expert <key id>
```
