# Backing up the GPG directory

To backup the GPG directory with restrictive access

```bash
$ umask 077; tar -cf $HOME/gnupg-backup`date +"%Y_%m_%d%H_%M_%S"`.tar -C $HOME .gnupg
```
