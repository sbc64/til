# Exporting gpg keys

Run the following to export the public key.

```bash
$ gpg --armor --export 040FE79B > voldermort.public.asc
```
