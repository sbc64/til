# Labels and selectors

Label and selector

Labels are a set of key/pair values, used to attach to objects. Labels are designed to specify meaningful, identifying information for the object. Common usage is micro-service name, tier, environment, and software version. Users could define meaningful labels that could be used with selector later. Labels syntax in object spec is:

```yaml
labels: 
  $key1: $value1 
  $key2: $value2 
```

Along with label, label selector is used to filter the set of objects. Separated by commas, multiple requirements will be joined by the AND logical operator. There are two ways to filter:

Equality-based requirement
Set-based requirement
Equality-based requirement supports the operator of =, ==, and !=. For example, if selector is chapter=2,version!=0.1, the result will be object C. If requirement is version=0.1, the result will be object A and object B. If we write the requirement in supported object spec, it'll be as follows:

```yaml
selector: 
  $key1: $value1
```


Set-based requirement supports in, notin, and exists (for key only). For example, if requirement is chapter in (3, 4),version, then object A will be returned. If requirement is version notin (0.2), !author_info, the result will be object A and object B. The following is an example if we write to the object spec that supports set-based requirement:

```yaml
selector: 
  matchLabels:  
    $key1: $value1 
  matchExpressions: 
{key: $key2, operator: In, values: [$value1, $value2]} 
```  
