# Docker volumes 

To create collapsable markdown segments one can use the following:

```markdown
## collapsible markdown?

<details><summary>CLICK ME</summary>
<p>

#### yes, even hidden code blocks!

```python
print("hello world!")
```

</p>
</details>
```
